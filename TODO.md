* In `Ordinals.v`, should we restrict our attention to limits of *stricly
  increasing* sequences? Without this feature, the constructors `zero` and
  `lim` overlap (the limit of the constant sequence `λ_.zero` is `zero`),
  so the ability to decide whether an element is zero is lost. Without it,
  defining exponentiation might be problematic, as pointed out by [Kraus et
  al.](https://arxiv.org/pdf/2208.03844.pdf)

* If we do *not* restrict our attention to increasing sequences, then `lim`
  should be renamed to `sup`.

* Define addition, multiplication, exponentiation.

* Define Cantor normal forms and their interpretation in terms of Brouwer
  trees.
