Require Import Unicode.Utf8.
Require Import Coq.Classes.Morphisms.                 (* Reflexive *)
Require Import Coq.Program.Basics.                    (* impl *)
Require Import Coq.Wellfounded.Inverse_Image.         (* wf_inverse_image *)
From SMBT Require Brouwer.

Local Set Warnings "-notation-overridden".

(* This file constructs the type of ordinals as a subset of the type
   of Brouwer trees: it is the subset of well-behaved trees. *)

(* The type of ordinals is equipped with construction operations: zero
   [zero], successor [succ], limit [lim], maximum [max]. It is also
   equipped with a preorder [≤], which induces an equivalence [≡]. The
   corresponding strict preorder [<] is well-founded. *)

(* The type of ordinals should be viewed by an end user as an abstract
   type. That is, the types and properties of the operations matter,
   but the definitions of these types and operations should be of no
   concern. *)

(* This file is based on the paper "Strictly Monotone Brouwer Trees for
   Well-Founded Recursion Over Multiple Arguments", by Joseph Eremondi
   (CPP 2024). It corresponds to Sections 2, 3, and 4.1. *)

(* For simplicity, we index all limits with the nonempty type [nat].
   The paper proposes a more elaborate construction where limits can
   be indexed with arbitrary (nonempty) types drawn from a universe. *)

(* -------------------------------------------------------------------- *)
(* -------------------------------------------------------------------- *)

(* Section 4.2. *)

(* -------------------------------------------------------------------- *)

(* An ordinal is a well-behaved Brouwer tree. *)

Definition ordinal : Type :=
  { t | Brouwer.wb t }.

(* -------------------------------------------------------------------- *)

(* Technical local tactics. *)

Local Ltac destruct_ordinals :=
  repeat match goal with o: ordinal |- _ => destruct o as [o ?] end.

Local Ltac unfold_whatever :=
  idtac.

Local Ltac unwrap :=
  intros; destruct_ordinals; unfold_whatever; simpl.

Local Ltac lift H :=
  unwrap; eauto using H.

(* -------------------------------------------------------------------- *)

(* The construction operations are [zero], [succ], and [lim]. *)

(* If [f : nat → ordinal] describes an infinite sequence of ordinals
   then [lim f] represents the limit, or (more accurately) the least
   upper bound of this sequence. *)

(* It is worth noting that [lim] is not a constructor, insofar as it
   is not disjoint with [zero] and [succ]. For example, the limit of
   the constant sequence that is every where [zero] is equivalent to
   [zero]. *)

(* For this reason, we do *not* provide a case analysis principle
   stating that an ordinal must be either zero, or a successor, or
   a limit. We could provide such a statement, with the caveat that
   the last case is not disjoint with the previous two cases. *)

Program Definition zero : ordinal :=
  Brouwer.Z.
Next Obligation. eapply Brouwer.wb_Z. Qed.

Program Definition succ (o : ordinal) : ordinal :=
  Brouwer.S o.
Next Obligation. lift Brouwer.wb_S. Qed.

Program Definition lim (f : nat → ordinal) : ordinal :=
  Brouwer.imax (Brouwer.lim f).
Next Obligation. eapply Brouwer.wb_imax. Qed.

(* -------------------------------------------------------------------- *)

(* A preorder on ordinals. *)

Program Definition le (t u : ordinal) : Prop :=
  Brouwer.le t u.

Notation "t ≤ u" :=
  (le t u) (at level 70).

Local Ltac unfold_whatever ::=
  unfold le.

(* [le] is a preorder. *)

Lemma le_reflexive : ∀ t, t ≤ t.
Proof. lift Brouwer.le_reflexive. Qed.

Lemma le_transitive : ∀ t u v, t ≤ u → u ≤ v → t ≤ v.
Proof. lift Brouwer.le_transitive. Qed.

(* Together with transitivity (above), the following four lemmas can
   be viewed as the constructors of the relation [≤]. *)

(* [le_cocone_refl] states that a limit is an upper bound for the
   defining sequence, that is, [lim f] lies above every [f k]. *)

(* [le_limiting] states that a limit is the least upper bound of
   its defining sequence. *)

Lemma le_zero : ∀ o,  zero ≤ o.
Proof. lift Brouwer.le_Z. Qed.

Lemma le_succ_mono : ∀ t u,  t ≤ u  →  succ t ≤ succ u.
Proof. lift Brouwer.le_S_mono. Qed.

Lemma le_cocone_refl : ∀ k f,  f k ≤ lim f.
Proof.
  unfold le, lim. intros k f. simpl.
  eapply Brouwer.le_cocone_refl_imax
    with (f := λ k, proj1_sig (f k)).
Qed.

Lemma le_limiting : ∀ o f,  (∀ k, f k ≤ o)  →  lim f ≤ o.
Proof. unfold lim. lift Brouwer.le_limiting_imax. Qed.

(* There follows that [lim] is compatible with pointwise [≤]. *)

Lemma ext_lim : ∀ f g,  (∀ k, f k ≤ g k)  →  lim f ≤ lim g.
Proof.
  unfold lim, le; simpl. intros.
  eapply Brouwer.imax_mono.
  eapply Brouwer.ext_lim.
  assumption.
Qed.

(* If every element of the sequence [g] lies above [t] then
   the limit of the sequence lies above [t] as well. *)

(* This lemma relies on the fact that the type [index] is inhabited. *)

Lemma under_lim : ∀ t g,  (∀ k, t ≤ g k)  →  t ≤ lim g.
Proof. lift Brouwer.under_lim_imax. Qed.

(* [t] lies below its successor [succ t]. *)

Lemma le_succ : ∀ t, t ≤ succ t.
Proof. lift Brouwer.le_S. Qed.

(* [succ] can be canceled out. *)

Lemma cancel_le_succ_succ : ∀ t u,  succ t ≤ succ u  →  t ≤ u.
Proof. unwrap. inversion H; subst. assumption. Qed.

(* [succ _ ≤ zero] is a contradiction. *)

Lemma cancel_le_succ_zero : ∀ t,  succ t ≤ zero  →  False.
Proof. unwrap. inversion H. Qed.

(* Type class instances, for use by the [rewrite] tactic. *)

Global Instance Reflexive_le : Reflexive le.
Proof. eauto using le_reflexive. Qed.

Global Instance Transitive_le : Transitive le.
Proof. eauto using le_transitive. Qed.

Global Instance PreOrder_le : PreOrder le.
Proof. econstructor; typeclasses eauto. Qed.

Global Instance Proper_le_le_succ : Proper (le ==> le) succ.
Proof. unfold Proper, respectful. eauto using le_succ_mono. Qed.

Global Instance Proper_pw_le_lim :
  Proper (pointwise_relation _ le ==> le) lim.
Proof. unfold Proper, respectful, pointwise_relation. exact ext_lim. Qed.

(* -------------------------------------------------------------------- *)

(* A strict preorder. *)

Program Definition lt (t u : ordinal) : Prop :=
  Brouwer.lt t u.

Notation "t < u" :=
  (lt t u).

Local Ltac unfold_whatever ::=
  unfold lt, le.

(* [t < u] is equivalent to [succ t ≤ u]. *)

Lemma lt_is_le : ∀ t u,  t < u ↔ succ t ≤ u.
Proof. unwrap. tauto. Qed.

(* [t] lies strictly below its successor [succ t]. *)

Lemma lt_succ : ∀ t, t < succ t.
Proof. intro. rewrite lt_is_le. reflexivity. Qed.

(* Strict ordering implies ordering. *)

Lemma lt_le : ∀ t u, t < u → t ≤ u. (* with a loss of information *)
Proof. intros t u. rewrite lt_is_le. eauto using le_succ, le_transitive. Qed.

Lemma lt_succ_le : ∀ t u, t < succ u → t ≤ u. (* with no loss of information *)
Proof. intros t u. rewrite lt_is_le. eauto using cancel_le_succ_succ. Qed.

(* Strict ordering is compatible with ordering. *)

Lemma lt_le_le : ∀ t u v, t < u → u ≤ v → t < v.
Proof. intros. rewrite lt_is_le. eauto using le_transitive. Qed.

Lemma le_lt_le : ∀ t u v, t ≤ u → u < v → t < v.
Proof. intros. rewrite lt_is_le. eauto using le_succ_mono, le_transitive. Qed.

(* Nothing is below zero. *)

Lemma cancel_lt_zero : ∀ t,  t < zero  →  False.
Proof. lift Brouwer.neg_lt_Z. Qed.

(* The strict preorder is irreflexive and transitive. *)

Lemma lt_irreflexive : ∀ t, t < t → False.
Proof. lift Brouwer.lt_irreflexive. Qed.

Lemma lt_transitive : ∀ t u v, t < u → u < v → t < v.
Proof. lift Brouwer.lt_transitive. Qed.

(* Type class instances, for use by [rewrite]. *)

Global Instance Irreflexive_lt : Irreflexive lt.
Proof. unfold Irreflexive, Reflexive, complement.
       eauto using lt_irreflexive. Qed.

Global Instance Transitive_lt : Transitive lt.
Proof. eauto using lt_transitive. Qed.

Global Instance StrictPreOrder_lt : StrictOrder lt.
Proof. econstructor; typeclasses eauto. Qed.

Global Instance subrelation_lt_le : subrelation lt le.
Proof. unfold subrelation. eauto using lt_le. Qed.

(* -------------------------------------------------------------------- *)

(* The equivalence relation [≡] is induced by the preorder. *)

Definition equiv t u :=
  t ≤ u /\ u ≤ t.

Notation "t ≡ u" :=
  (equiv t u) (at level 80).

(* Equivalence is not equality, because the limit of a sequence is not
   necessarily strictly greater than every element of the sequence. For
   instance, the limit of the constant function Z is equivalent to Z
   according to the preorder, yet these two ordinals are syntactically
   distinct. *)

Goal zero ≡ lim (fun _ => zero).
Proof. split; eauto using le_zero, le_limiting. Qed.

(* [≡] is an equivalence. *)

Lemma equiv_reflexive : ∀ t, t ≡ t.
Proof. split; eauto using le_reflexive. Qed.

Lemma equiv_symmetric : ∀ t u, t ≡ u → u ≡ t.
Proof. unfold equiv; tauto. Qed.

Lemma equiv_transitive : ∀ t u v, t ≡ u → u ≡ v → t ≡ v.
Proof. unfold equiv; intuition eauto using le_transitive. Qed.

(* Equivalence implies preorder. *)

Lemma equiv_implies_le : ∀ t u, t ≡ u → t ≤ u.
Proof. unfold equiv; tauto. Qed.

(* Type class instances, for use by [rewrite]. *)

Local Instance Reflexive_equiv : Reflexive equiv.
Proof. eauto using equiv_reflexive. Qed.

Local Instance Symmetric_equiv : Symmetric equiv.
Proof. eauto using equiv_symmetric. Qed.

Local Instance Transitive_equiv : Transitive equiv.
Proof. eauto using equiv_transitive. Qed.

Local Instance Equivalence_equiv : Equivalence equiv.
Proof. split; typeclasses eauto. Qed.

Local Instance subrelation_equiv_le : subrelation equiv le.
Proof. unfold subrelation. eauto using equiv_implies_le. Qed.

Local Instance Proper_equiv_equiv_impl_le :
  Proper (equiv ==> equiv ==> impl) le.
Proof.
  unfold Proper, respectful, impl.
  eauto 6 using equiv_implies_le, equiv_symmetric, le_transitive.
Qed.

Local Instance Proper_equiv_equiv_impl_lt :
  Proper (equiv ==> equiv ==> impl) lt.
Proof.
  unfold Proper, respectful, impl.
  intros t u Htu t' u' Ht'u' Htt'.
  rewrite lt_is_le in *.
  rewrite <- Htu.
  rewrite <- Ht'u'.
  assumption.
Qed.

Lemma equiv_lim_lim : ∀ f g,  (∀ k, f k ≡ g k)  →  lim f ≡ lim g.
Proof. split; eauto using ext_lim, equiv_implies_le, equiv_symmetric. Qed.

Lemma ext_lim_equiv : ∀ f g,  (∀ k, f k ≡ g k)  →  lim f ≡ lim g.
Proof.
  split; eapply ext_lim; eauto using equiv_implies_le, equiv_symmetric.
Qed.

(* A trivial technical result: equivalence on ordinals coincides with
   equivalence on the underlying Brouwer trees. *)

Local Lemma equiv_equiv :
  ∀ t u,
  Brouwer.equiv (proj1_sig t) (proj1_sig u) →
  t ≡ u.
Proof. unwrap. eauto. Qed.

(* -------------------------------------------------------------------- *)

(* The strict preorder is well-founded. *)

Lemma wf_lt :
  well_founded lt.
Proof.
  unfold lt. eapply wf_inverse_image. (* wow! *) eapply Brouwer.wf_lt.
Qed.

(* -------------------------------------------------------------------- *)

(* [max t u] is the least upper bound of [t] and [u]. *)

Program Definition max (t u : ordinal) : ordinal :=
  Brouwer.max t u.
Next Obligation. lift Brouwer.wb_max. Qed.

Local Ltac unfold_whatever ::=
  unfold le, lt, max; try simple eapply equiv_equiv.

(* [max] is commutative. *)

Lemma max_comm_le : ∀ t u,  max t u ≤ max u t.
Proof. lift Brouwer.max_comm_le. Qed.

Lemma max_comm : ∀ t u,  max t u ≡ max u t.
Proof. split; eauto using max_comm_le. Qed.

(* The following equalities characterize the function [max]. *)

Lemma max_zero_u : ∀ u,  max zero u ≡ u.
Proof.
  unwrap. rewrite Brouwer.max_Z_u. apply Brouwer.equiv_reflexive. Qed.

Lemma max_t_zero : ∀ t,  max t zero ≡ t.
Proof. unwrap. rewrite Brouwer.max_t_Z. apply Brouwer.equiv_reflexive. Qed.

Lemma max_succ_succ : ∀ t u,  max (succ t) (succ u) ≡ succ (max t u).
Proof. unwrap. rewrite Brouwer.max_S_S. apply Brouwer.equiv_reflexive. Qed.

Lemma max_lim_u : ∀ f u,  max (lim f) u ≡ lim (fun k => max (f k) u).
Proof. lift Brouwer.max_imax_lim_u. Qed.

Lemma max_t_lim : ∀ t g,  max t (lim g) ≡ lim (fun k => max t (g k)).
Proof.
  intros. rewrite max_comm. rewrite max_lim_u.
  eapply ext_lim_equiv; intro k.
  eapply max_comm.
Qed.

Lemma max_lim_lim' :
  ∀ f g,
  max (lim f) (lim g) ≡
  lim (fun k => lim (fun k' => max (f k) (g k'))).
Proof.
  intros.
  rewrite max_lim_u.
  eapply ext_lim_equiv; intro.
  rewrite max_t_lim.
  eapply ext_lim_equiv; intro.
  reflexivity.
Qed.

(* [max t u] lies above [t] and [u]. *)

(* The fact that [max t u] is the least upper bound of [t] and [u]
   is proved further on; see [max_is_lub]. *)

Lemma max_le_left : ∀ t u,  t ≤ max t u.
Proof. lift Brouwer.max_le_left. Qed.

Lemma max_le_right : ∀ t u,  u ≤ max t u.
Proof. lift Brouwer.max_le_right. Qed.

(* [max] is monotonic in each of its arguments. *)

Lemma max_mono : ∀ t u t' u',  t ≤ t'  →  u ≤ u'  →  max t u ≤ max t' u'.
Proof. lift Brouwer.max_mono. Qed.

Lemma max_mono_strict : ∀ t u t' u',  t < t'  →  u < u'  → max t u < max t' u'.
Proof. lift Brouwer.max_mono_strict. Qed.

(* [max] is associative. *)

Lemma max_assoc_left : ∀ t u v,  max t (max u v) ≤ max (max t u) v.
Proof. lift Brouwer.max_assoc_left. Qed.

Lemma max_assoc_right : ∀ t u v,  max (max t u) v ≤ max t (max u v).
Proof. lift Brouwer.max_assoc_right. Qed.

Lemma max_assoc : ∀ t u v,  max t (max u v) ≡ max (max t u) v.
Proof. split; eauto using max_assoc_left, max_assoc_right. Qed.

(* [max] is idempotent. *)

Lemma max_idem : ∀ t,  max t t ≡ t.
Proof.
  split.
  { exact (proj2_sig t). } (* well-behavedness is exploited here *)
  { eapply max_le_right. }
Qed.

(* Set up rewriting under [max]. *)

Local Instance Proper_equiv_equiv_equiv_max :
  Proper (equiv ==> equiv ==> equiv) max.
Proof.
  unfold Proper, respectful.
  split; eauto 6 using max_mono, equiv_implies_le, equiv_symmetric.
Qed.

Global Instance Proper_le_le_le_max : Proper (le ==> le ==> le) max.
Proof. unfold Proper, respectful. eauto using max_mono. Qed.

(* [max t u] is the least upper bound of [t] and [u]. *)

Lemma max_is_lub : ∀ t u v,  t ≤ v  →  u ≤ v  →  max t u ≤ v.
Proof.
  intros t u v Htv Huv.
  rewrite Htv, Huv.
  rewrite max_idem. (* idempotence of [max] is exploited here *)
  reflexivity.
Qed.

(* -------------------------------------------------------------------- *)

(* [max] is in fact equivalent to [max'], a maximum function that is
   defined using [lim]. *)

Definition max' t u :=
  lim (fun n => match n with 0 => t | S _ => u end).

Local Lemma max'_le_left : ∀ t u, t ≤ max' t u.
Proof.
  intros. unfold max'.
  set (f := (fun n => match n with 0 => t | S _ => u end)).
  change t with (f 0).
  eapply le_cocone_refl.
Qed.

Local Lemma max'_le_right : ∀ t u, u ≤ max' t u.
Proof.
  intros. unfold max'.
  set (f := (fun n => match n with 0 => t | S _ => u end)).
  change u with (f 1).
  eapply le_cocone_refl.
Qed.

Local Lemma max'_is_lub : ∀ t u v,  t ≤ v  →  u ≤ v  →  max' t u ≤ v.
Proof.
  intros. unfold max'.
  set (f := (fun n => match n with 0 => t | S _ => u end)).
  eapply le_limiting; intros [|]; simpl; assumption.
Qed.

Lemma max'_is_max : ∀ t u,  max' t u ≡ max t u.
Proof.
  split.
  { eapply max'_is_lub; eauto using max_le_left, max_le_right. }
  { eapply max_is_lub; eauto using max'_le_left, max'_le_right. }
Qed.

(* -------------------------------------------------------------------- *)

(* An injection of natural numbers into ordinals. *)

Fixpoint inj (n : nat) : ordinal :=
  match n with
  | 0   => zero
  | S n => succ (inj n)
  end.

(* The ordinal [ω]. *)

Definition omega :=
  lim inj.

(* [ω] is also the limit of the sequence [succ (inj _)]. *)

Definition succ_inj n :=
  succ (inj n).

Local Lemma lim_succ_inj_is_omega :
  lim succ_inj ≡ omega.
Proof.
  unfold omega. split.
  { eapply le_limiting; intro k.
    change (succ_inj k) with (inj (S k)).
    eapply le_cocone_refl. }
  { eapply ext_lim; intro k.
    eapply le_succ. }
Qed.

(* -------------------------------------------------------------------- *)

(* One might think that [succ] commutes with [lim]. This property,
   however, is false. If it were true, then [succ omega ≤ omega]
   would follow, which would imply a contradiction. *)

Goal
  ~ ∀ f, succ (lim f) ≡ lim (fun k => succ (f k)).
Proof.
  intro H.
  assert (omega < omega).
  { rewrite lt_is_le.
   rewrite <- lim_succ_inj_is_omega at 2. eapply H. }
  eauto using lt_irreflexive.
Qed.

(* For the same reason, the [lim] operator is not compatible with
   the strict preorder [<]. *)

Goal ~ ∀ f g, (∀ k, f k < g k) → lim f < lim g.
Proof.
  intro H.
  assert (omega < omega).
  { rewrite <- lim_succ_inj_is_omega at 2. eapply H.
    intro k. unfold succ_inj. eapply lt_succ. }
  eauto using lt_irreflexive.
Qed.

(* The following weaker property holds. *)

Lemma ext_lim_lt : ∀ f g, (∀ k, f k < g k) → lim f ≤ lim g.
Proof.
  intros f g H.
  eapply ext_lim; intro k.
  eauto using lt_le.
Qed.

(* -------------------------------------------------------------------- *)

(* Make our definitions abstract. *)

Global Opaque ordinal zero succ lim max le lt equiv.
