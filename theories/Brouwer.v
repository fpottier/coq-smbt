Require Import Unicode.Utf8.
Require Import Coq.Classes.Morphisms.                 (* Reflexive *)
Require Import Coq.Program.Basics.                    (* impl *)
Require Import Coq.Program.Equality.                  (* dependent induction *)
From Equations Require Import Equations.              (* Equations *)
Require Import Coq.Relations.Relation_Operators.      (* slexprod *)
Require Import Coq.Wellfounded.Lexicographic_Product. (* wf_slexprod *)

Local Set Warnings "-notation-overridden".

(* This file is not intended for end users. It serves as an intermediate
   step in the construction of ordinals. See Ordinals.v. *)

(* This file is based on the paper "Strictly Monotone Brouwer Trees for
   Well-Founded Recursion Over Multiple Arguments", by Joseph Eremondi
   (CPP 2024). It corresponds to Sections 2, 3, and 4.1. *)

(* For simplicity, we index all limits with the nonempty type [nat].
   The paper proposes a more elaborate construction where limits can
   be indexed with arbitrary (nonempty) types drawn from a universe. *)

(* -------------------------------------------------------------------- *)
(* -------------------------------------------------------------------- *)

(* Section 2.1. *)

(* -------------------------------------------------------------------- *)

(* All limits are indexed by [nat]. *)

Notation index :=
  nat.

Implicit Type k : index.

(* This type is inhabited. This is important, because empty limits are
   not well-behaved: see the lemma [under_lim]. *)

Definition inhabitant : index :=
  0.

(* -------------------------------------------------------------------- *)

(* Brouwer trees. *)

Inductive tree :=
| Z   : tree
| S   : tree → tree
| lim : (index → tree) → tree.

Implicit Type t u v : tree.
Implicit Type f g : index → tree.

(* -------------------------------------------------------------------- *)

(* A preorder on Brouwer trees. *)

Reserved Notation "A ≤ B" (at level 70).

Inductive le : tree → tree → Prop :=
| le_Z :
    ∀ t,
    Z ≤ t
| le_S_mono :
    ∀ t1 t2,
    t1 ≤ t2 →
    S t1 ≤ S t2
| le_cocone :
    ∀ k t f,
    t ≤ f k →
    t ≤ lim f
| le_limiting :
    ∀ t f,
    (∀ k, f k ≤ t) →
    lim f ≤ t

where "t1 ≤ t2" := (le t1 t2).

Local Hint Constructors le : le.

(* [le] is a preorder. *)

Lemma le_reflexive : ∀ t, t ≤ t.
Proof. induction t; eauto with le. Qed.

Lemma le_transitive : ∀ t u, t ≤ u → ∀ v, u ≤ v → t ≤ v.
Proof.
  induction 1; intros v Huv;
  solve [ eauto with le | dependent induction Huv; eauto with le ].
Qed.

(* A limit lies above every element of its defining sequence. *)

(* This is just a variant of [le_cocone] without built-in transitivity. *)

Lemma le_cocone_refl : ∀ f k, f k ≤ lim f.
Proof. eauto using le_cocone, le_reflexive. Qed.

(* The limits of two related sequences are related. *)

Lemma ext_lim : ∀ f g,  (∀ k, f k ≤ g k)  →  lim f ≤ lim g.
Proof.
  (* It suffices to show that [lim g] is above every [f k]. This itself
     follows from the fact that [g k] is above [f k]. *)
  eauto with le.
Qed.

(* The lemma [under_lim] relies on the fact that the [index] type is
   inhabited. Indeed, if the index type is empty, then [lim g] is an
   empty limit, so it is equivalent to [Z]. Yet [t] is not necessarily
   [Z], so [t ≤ lim g] is false. *)

Lemma under_lim : ∀ t g,  (∀ k, t ≤ g k)  →  t ≤ lim g.
Proof. eauto using (le_cocone inhabitant). Qed.

(* [t] lies below its successor [succ t]. *)

Lemma le_S : ∀ t, t ≤ S t.
Proof. induction t; eauto using le_transitive, le_cocone_refl with le. Qed.

(* Type class instances, for use by the [rewrite] tactic. *)

Local Instance Reflexive_le : Reflexive le.
Proof. eauto using le_reflexive. Qed.

Local Instance Transitive_le : Transitive le.
Proof. eauto using le_transitive. Qed.

Local Instance PreOrder_le : PreOrder le.
Proof. econstructor; typeclasses eauto. Qed.

Local Instance Proper_le_le_S : Proper (le ==> le) S.
Proof. unfold Proper, respectful. eauto with le. Qed.

Local Instance Proper_pw_le_lim : Proper (pointwise_relation _ le ==> le) lim.
Proof. unfold Proper, respectful, pointwise_relation. exact ext_lim. Qed.

(* -------------------------------------------------------------------- *)

(* A strict preorder. *)

(* By definition, [t < u] is equivalent to [S t ≤ u]. *)

Definition lt t u :=
  S t ≤ u.

Notation "t < u" :=
  (lt t u).

(* Strict ordering implies ordering. *)

Lemma lt_le : ∀ t u, t < u → t ≤ u. (* with a loss of information *)
Proof. unfold lt. eauto using le_S, le_transitive. Qed.

Lemma lt_S_le : ∀ t u, t < S u → t ≤ u. (* with no loss of information *)
Proof. unfold lt. intros t u Htu. dependent destruction Htu. eauto. Qed.

(* Strict ordering is compatible with ordering. *)

Lemma lt_le_le : ∀ t u v, t < u → u ≤ v → t < v.
Proof. unfold lt. eauto using le_transitive. Qed.

Lemma le_lt_le : ∀ t u v, t ≤ u → u < v → t < v.
Proof. unfold lt. eauto using le_S_mono, le_transitive. Qed.

(* Nothing is below zero. *)

Lemma neg_lt_Z : ∀ t, t < Z → False.
Proof. unfold lt. inversion 1. Qed.

(* [t] lies strictly below its successor [succ t]. *)

Lemma lt_S : ∀ t, t < S t.
Proof. unfold lt. reflexivity. Qed.

(* The strict preorder is irreflexive and transitive. *)

Lemma lt_irreflexive : ∀ t, t < t → False.
Proof.
  unfold lt. induction t; intros Ht; dependent destruction Ht;
  eauto using le_transitive, le_cocone_refl, le_S_mono.
Qed.

Lemma lt_transitive : ∀ t u v, t < u → u < v → t < v.
Proof. unfold lt. eauto using le_transitive, le_S with le. Qed.

(* Type class instances, for use by [rewrite]. *)

Local Instance Irreflexive_lt : Irreflexive lt.
Proof. unfold Irreflexive, Reflexive, complement.
       eauto using lt_irreflexive. Qed.

Local Instance Transitive_lt : Transitive lt.
Proof. eauto using lt_transitive. Qed.

Local Instance StrictPreOrder_lt : StrictOrder lt.
Proof. econstructor; typeclasses eauto. Qed.

Local Instance subrelation_lt_le : subrelation lt le.
Proof. unfold subrelation. eauto using lt_le. Qed.

(* -------------------------------------------------------------------- *)

(* The equivalence relation [≡] is induced by the preorder. *)

Definition equiv t u :=
  t ≤ u /\ u ≤ t.

Notation "t ≡ u" :=
  (equiv t u) (at level 80).

(* Equivalence is not equality, because the limit of a sequence is not
   necessarily strictly greater than every element of the sequence. For
   instance, the limit of the constant function Z is equivalent to Z
   according to the preorder, yet these two ordinals are syntactically
   distinct. *)

Goal Z ≡ lim (fun _ => Z).
Proof. split; eauto with le. Qed.

(* [≡] is an equivalence. *)

Lemma equiv_reflexive : ∀ t, t ≡ t.
Proof. split; eauto using le_reflexive. Qed.

Lemma equiv_symmetric : ∀ t u, t ≡ u → u ≡ t.
Proof. unfold equiv; tauto. Qed.

Lemma equiv_transitive : ∀ t u v, t ≡ u → u ≡ v → t ≡ v.
Proof. unfold equiv; intuition eauto using le_transitive. Qed.

(* Equivalence implies preorder. *)

Lemma equiv_implies_le : ∀ t u, t ≡ u → t ≤ u.
Proof. unfold equiv; tauto. Qed.

(* Type class instances, for use by [rewrite]. *)

Local Instance Reflexive_equiv : Reflexive equiv.
Proof. eauto using equiv_reflexive. Qed.

Local Instance Symmetric_equiv : Symmetric equiv.
Proof. eauto using equiv_symmetric. Qed.

Local Instance Transitive_equiv : Transitive equiv.
Proof. eauto using equiv_transitive. Qed.

Local Instance Equivalence_equiv : Equivalence equiv.
Proof. split; typeclasses eauto. Qed.

Local Instance subrelation_equiv_le : subrelation equiv le.
Proof. unfold subrelation. eauto using equiv_implies_le. Qed.

Local Instance Proper_equiv_equiv_impl_le :
  Proper (equiv ==> equiv ==> impl) le.
Proof.
  unfold Proper, respectful, impl.
  eauto 6 using equiv_implies_le, equiv_symmetric, le_transitive.
Qed.

Lemma equiv_lim_lim : ∀ f g,  (∀ k, f k ≡ g k)  →  lim f ≡ lim g.
Proof. split; eauto using ext_lim, equiv_implies_le, equiv_symmetric. Qed.

Lemma ext_lim_equiv : ∀ f g,  (∀ k, f k ≡ g k)  →  lim f ≡ lim g.
Proof.
  split; eapply ext_lim; eauto using equiv_implies_le, equiv_symmetric.
Qed.

(* -------------------------------------------------------------------- *)
(* -------------------------------------------------------------------- *)

(* Section 2.2. *)

(* -------------------------------------------------------------------- *)

(* [accessible t] means that [t] is accessible with respect to the strict
   preorder [<]. In other words, this means that there exists no infinite
   descending chain out of [t]. *)

Local Notation accessible t :=
  (Acc lt t).

(* If [u] is accessible, then every [t] such that [t ≤ u] holds is also
   accessible. *)

Lemma le_accessible :
  ∀ u, accessible u →
  ∀ t, t ≤ u → accessible t.
Proof.
  intros u Hu. dependent destruction Hu.
  intros t Htu.
  econstructor. intros s Hst.
  eauto using lt_le_le.
Qed.

(* All terms are accessible. *)

(* In other words, the strict preorder is well-founded. *)

Lemma wf_lt :
  well_founded lt.
Proof.
  unfold well_founded.
  intro t; induction t; econstructor; intros u Hu.
  { exfalso. eauto using neg_lt_Z. }
  { eauto using le_accessible, lt_S_le. }
  { unfold lt in Hu.
    (* The only way to derive [S u ≤ lim t] is via [le_cocone].
       Inversion yields a concrete [k] on which we can recur. *)
    dependent destruction Hu.
    eauto using le_accessible, le_S, le_transitive. }
Qed.

(* -------------------------------------------------------------------- *)
(* -------------------------------------------------------------------- *)

(* Section 3.2. *)

(* -------------------------------------------------------------------- *)

(* The following boilerplate is used in the definition of [max]. *)

(* Define the subterm ordering on trees, and generate a proof of its
   well-foundedness. (This could be done by hand.) *)

Derive NoConfusion for tree.
Derive Subterm for tree.

(* Define the subterm ordering on pairs of trees, using lexicographic
   product, and prove that it is well-founded. *)

Definition smaller_trees :=
  slexprod _ _ tree_subterm tree_subterm.

Local Instance wf_smaller_trees :
  WellFounded smaller_trees.
Proof.
  apply wf_slexprod; apply well_founded_tree_subterm.
Qed.

(* -------------------------------------------------------------------- *)

(* A recursive definition of [max]. *)

(* This function is named [indMax] in the paper. *)

Equations max (t u : tree) : tree by wf (t, u) smaller_trees :=
  max t Z         := t ;
  max Z u         := u ;
  max (S t) (S u) := S (max t u) ;
  max (lim f) u   := lim (fun k => max (f k) u) ;
  max t (lim g)   := lim (fun k => max t (g k)) .

Next Obligation. left. repeat econstructor. Qed.
Next Obligation. left. repeat econstructor. Qed.
Next Obligation. right. repeat econstructor. Qed.
Next Obligation. left. repeat econstructor. Qed.

(* -------------------------------------------------------------------- *)

(* The following equalities paraphrase the definition of [max]. *)

Transparent max_unfold.

Local Ltac max_spec :=
  rewrite max_unfold_eq; simpl max_unfold.

Lemma max_Z_u :
  ∀ u,
  max Z u = u.
Proof.
  intros. destruct u; max_spec; reflexivity.
Qed.

Lemma max_t_Z :
  ∀ t,
  max t Z = t.
Proof.
  intros. destruct t; max_spec; reflexivity.
Qed.

Lemma max_S_S :
  ∀ t u,
  max (S t) (S u) = S (max t u).
Proof.
  intros. max_spec. reflexivity.
Qed.

Lemma max_S_lim :
  ∀ t g,
  max (S t) (lim g) = lim (fun k => max (S t) (g k)).
Proof.
  intros. max_spec. reflexivity.
Qed.

Lemma max_lim_S :
  ∀ f u,
  max (lim f) (S u) = lim (fun k => max (f k) (S u)).
Proof.
  intros. max_spec. reflexivity.
Qed.

Lemma max_lim_lim :
  ∀ f g,
  max (lim f) (lim g) = lim (fun k => max (f k) (lim g)).
Proof.
  intros. max_spec. reflexivity.
Qed.

Lemma max_lim_u :
  ∀ f u,
  max (lim f) u ≡ lim (fun k => max (f k) u).
Proof.
  destruct u.
  { rewrite max_t_Z. apply equiv_lim_lim; intro.
    rewrite max_t_Z. apply equiv_reflexive. }
  { rewrite max_lim_S. apply equiv_reflexive. }
  { rewrite max_lim_lim. apply equiv_reflexive. }
Qed.

(* The symmetric form of [max_lim_u], named [max_t_lim], is proved
   later on, after we establish that [max] is commutative. *)

(* -------------------------------------------------------------------- *)

(* The following induction principle reflects the shape of the
   definition of [max], with just 5 branches. It is more natural than
   the induction principle generated by Equations, named [max_elim],
   which creates 7 branches. *)

Lemma max_elim_optimised :
  ∀ P : tree → tree → tree → Type,
  (∀ t,
    P t Z t
  ) →
  (∀ u,
    P Z u u
  ) →
  (∀ t u,
    P t u (max t u) →
    P (S t) (S u) (S (max t u))) →
  (∀ f u,
    (∀ k, P (f k) u (max (f k) u)) →
    P (lim f) u (lim (fun k => max (f k) u))
  ) →
  (∀ t g,
    (∀ k, P t (g k) (max t (g k))) →
    P t (lim g) (lim (fun k => max t (g k)))
  ) →
  ∀ t u,
    P t u (max t u).
Proof.
  intros. apply max_elim; eauto.
Qed.

(* -------------------------------------------------------------------- *)

(* [max t u] lies above [t] and [u]. *)

Lemma max_le_left : ∀ t u,  t ≤ max t u.
Proof.
  intros t u. eapply max_elim_optimised; clear t u;
  eauto using le_reflexive, under_lim with le.
Qed.

Lemma max_le_right : ∀ t u,  u ≤ max t u.
Proof.
  intros t u. eapply max_elim_optimised; clear t u;
  eauto using le_reflexive, under_lim with le.
Qed.

(* [max] is monotonic in each of its arguments. *)

Lemma max_mono_right : ∀ t u u',  u ≤ u'  →  max t u ≤ max t u'.
Proof.
  intros t u. eapply max_elim_optimised; clear t u.
  { intros. eapply max_le_left. }
  { intros. rewrite max_Z_u. assumption. }
  { intros t u IH u' Hle. dependent induction Hle.
    + clear IHHle. rewrite max_S_S. eapply le_S_mono. eauto.
    + (* Simplify the induction hypothesis IHHle,
         which seems more general than needed. *)
      specialize (IHHle _ IH eq_refl). clear IH.
      rewrite max_S_lim. eapply le_cocone. exact IHHle. }
  { intros f u IH u' Hle. eapply le_limiting; intro.
    rewrite IH by eauto.
    (* A special case of [max_mono_left] suffices here. *)
    rewrite max_lim_u.
    eapply le_cocone_refl with (f := fun k => max (f k) _). }
  { intros. eauto using le_limiting, le_transitive, le_cocone_refl. }
Qed.

Lemma max_mono_left : ∀ t u t',  t ≤ t'  →  max t u ≤ max t' u.
Proof.
  intros t u. eapply max_elim_optimised; clear t u.
  { intros. rewrite max_t_Z. assumption. }
  { intros. eapply max_le_right. }
  { intros t u IH t' Hle. dependent induction Hle.
    + clear IHHle. rewrite max_S_S. eapply le_S_mono. eauto.
    + (* Simplify the induction hypothesis IHle,
         which seems more general than needed. *)
      specialize (IHHle _ IH eq_refl). clear IH.
      rewrite max_lim_S. eapply le_cocone. exact IHHle. }
  { intros. eauto using le_limiting, le_transitive, le_cocone_refl. }
  { intros t g IH t' ?. eapply le_limiting; intro.
    rewrite IH by eauto.
    (* Just use [max_mono_right], which we have already proved. *)
    eauto using max_mono_right, le_cocone_refl. }
Qed.

Lemma max_mono : ∀ t u t' u',  t ≤ t'  →  u ≤ u'  →  max t u ≤ max t' u'.
Proof.
  eauto using max_mono_left, max_mono_right, le_transitive.
Qed.

Lemma max_mono_strict : ∀ t u t' u',  t < t'  →  u < u'  → max t u < max t' u'.
Proof.
  unfold lt. intros. rewrite <- max_S_S. eauto using max_mono.
Qed.

(* Set up rewriting under [max]. *)

Local Instance Proper_equiv_equiv_equiv_max :
  Proper (equiv ==> equiv ==> equiv) max.
Proof.
  unfold Proper, respectful.
  split; eauto 6 using max_mono, equiv_implies_le, equiv_symmetric.
Qed.

Local Instance Proper_le_le_le_max : Proper (le ==> le ==> le) max.
Proof. unfold Proper, respectful. eauto using max_mono. Qed.

(* -------------------------------------------------------------------- *)

(* [max] is commutative. *)

Lemma max_comm_le : ∀ t u,  max t u ≤ max u t.
Proof.
  intros t u. eapply max_elim_optimised; clear t u.
  { intros. rewrite max_Z_u. reflexivity. }
  { intros. rewrite max_t_Z. reflexivity. }
  { intros. rewrite max_S_S. eapply le_S_mono. eauto. }
  { intros f u IH.
    eapply le_limiting; intro.
    rewrite IH. eapply max_mono_right. eapply le_cocone_refl. }
  { intros t g IH.
    eapply le_limiting; intro.
    rewrite IH. eapply max_mono_left. eapply le_cocone_refl. }
Qed.

Lemma max_comm : ∀ t u,  max t u ≡ max u t.
Proof.
  split; eauto using max_comm_le.
Qed.

(* -------------------------------------------------------------------- *)

(* We can now establish the symmetric counterpart of [max_lim_u], *)

Lemma max_t_lim : ∀ t g,  max t (lim g) ≡ lim (fun k => max t (g k)).
Proof.
  intros. rewrite max_comm, max_lim_u.
  eapply ext_lim_equiv; intro.
  rewrite max_comm. reflexivity.
Qed.

(* and a symmetric version of [max_lim_lim], where both limits are
   hoisted out of [max]. *)

Lemma max_lim_lim' :
  ∀ f g,
  max (lim f) (lim g) ≡
  lim (fun k => lim (fun k' => max (f k) (g k'))).
Proof.
  intros.
  rewrite max_lim_lim.
  eapply ext_lim_equiv; intro.
  rewrite max_t_lim.
  eapply ext_lim_equiv; intro.
  reflexivity.
Qed.

(* -------------------------------------------------------------------- *)

(* [max] is associative. *)

Lemma max_assoc_left : ∀ t u v,  max t (max u v) ≤ max (max t u) v.
Proof.
  intros t u. eapply max_elim_optimised; clear t u.
  { intros. rewrite max_Z_u. reflexivity. }
  { intros. rewrite max_Z_u. reflexivity. }
  { intros. induction v.
    + rewrite !max_t_Z. rewrite max_S_S. reflexivity.
    + rewrite !max_S_S. eapply le_S_mono. eauto.
    + rewrite !max_S_lim. eapply ext_lim; intro. eauto. }
  { intros f u IH v.
    rewrite max_lim_u, max_lim_u.
    eapply ext_lim; intro. eapply IH. }
  { intros t g IH v.
    rewrite max_lim_u, max_lim_u, max_t_lim.
    eapply ext_lim; intro. eapply IH. }
Qed.

Lemma max_assoc_right : ∀ t u v,  max (max t u) v ≤ max t (max u v).
Proof.
  intros t u. eapply max_elim_optimised; clear t u.
  { intros. rewrite max_Z_u. reflexivity. }
  { intros. rewrite max_Z_u. reflexivity. }
  { intros. induction v.
    + rewrite !max_t_Z. rewrite max_S_S. reflexivity.
    + rewrite !max_S_S. eapply le_S_mono. eauto.
    + rewrite !max_S_lim. eapply ext_lim; intro. eauto. }
  { intros f u IH v.
    rewrite max_lim_u, max_lim_u.
    eapply ext_lim; intro. eapply IH. }
  { intros t g IH v.
    rewrite max_lim_u, max_lim_u, max_t_lim.
    eapply ext_lim; intro. eapply IH. }
Qed.

Lemma max_assoc : ∀ t u v,  max t (max u v) ≡ max (max t u) v.
Proof.
  split; eauto using max_assoc_left, max_assoc_right.
Qed.

(* -------------------------------------------------------------------- *)
(* -------------------------------------------------------------------- *)

(* Section 4.1. *)

(* -------------------------------------------------------------------- *)

(* [nmax t n] is the iterated application of [max _ t],
   [n] times, to [t]. *)

(* This function is named [nindMax] in the paper. *)

Fixpoint nmax (t : tree) (n : nat) : tree :=
  match n with
  | 0 =>
      t
  | Datatypes.S n =>
      max (nmax t n) t
  end.

(* [imax t] is the limit of [nmax t _].
   It can be thought of as the iterated application of [max _ t],
   an infinite number of times, to [t]. *)

(* This function is named [indMax∞] in the paper. *)

Definition imax (t : tree) : tree :=
  lim (fun k => nmax t k).

(* -------------------------------------------------------------------- *)

(* A series of technical results. *)

Lemma nmax_le_imax : ∀ t n, nmax t n ≤ imax t.
Proof.
  unfold imax. eauto using le_cocone_refl.
Qed.

Lemma self_le_imax : ∀ t, t ≤ imax t.
Proof.
  intros. eapply nmax_le_imax with (n := 0).
Qed.

Lemma imax_mono : ∀ t t', t ≤ t' → imax t ≤ imax t'.
Proof.
  unfold imax. intros. eapply ext_lim; intro k.
  induction k; simpl nmax; eauto using max_mono.
Qed.

Lemma imax_lt1 : ∀ t, max (imax t) t ≤ imax t.
Proof.
  intros. unfold imax at 1. rewrite max_lim_u.
  eapply le_limiting; intro k.
  eapply nmax_le_imax with (n := Datatypes.S k).
Qed.

Lemma imax_ltn : ∀ n t, max (imax t) (nmax t n) ≤ imax t.
Proof.
  induction n; simpl nmax; intros.
  { eapply imax_lt1. }
  { rewrite max_assoc. rewrite IHn. eapply imax_lt1. }
Qed.

(* [max] is idempotent on the subset of trees produced by [imax].
   In other words, every tree of the form [imax t] is well-behaved,
   in the sense defined below. *)

Lemma imax_idem : ∀ t, max (imax t) (imax t) ≤ imax t.
Proof.
  intros. unfold imax at 2. rewrite max_t_lim.
  eapply le_limiting; intro k.
  eapply imax_ltn.
Qed.

Lemma nmax_le_self : ∀ n t,  max t t ≤ t  →  nmax t n ≤ t.
Proof.
  induction n; simpl nmax; intros.
  { reflexivity. }
  { rewrite IHn by assumption. assumption. }
Qed.

Lemma imax_le_self : ∀ t,  max t t ≤ t  →  imax t ≤ t.
Proof.
  intros. unfold imax. eapply le_limiting; intro k.
  eauto using nmax_le_self.
Qed.

(* -------------------------------------------------------------------- *)

(* A Brouwer tree [t] is well-behaved if [max t t ≤ t] holds. *)

(* In Ordinals.v, an "ordinal" is defined as a well-behaved tree. *)

Definition wb t :=
  max t t ≤ t.

(* Well-behavedness implies [max t t ≡ t]. *)

Lemma wb_equiv : ∀ t, wb t → max t t ≡ t.
Proof.
  unfold wb. split; eauto using max_le_left.
Qed.

(* Zero, successor, and [max] preserve well-behavedness. *)

(* The constructor [lim] does *not* preserve well-behavedness.
   To compensate for this problem, [imax] will be used. *)

Lemma wb_Z : wb Z.
Proof.
  unfold wb. rewrite max_Z_u. reflexivity.
Qed.

Lemma wb_S : ∀ t, wb t → wb (S t).
Proof.
  unfold wb. intros t Ht. rewrite max_S_S. rewrite Ht. reflexivity.
Qed.

Lemma wb_max : ∀ t u, wb t → wb u → wb (max t u).
Proof.
  unfold wb. intros t u Ht Hu.
  (* This manual proof is quite painful! *)
  rewrite (max_comm t u) at 1.
  rewrite max_assoc.
  rewrite <- (max_assoc u t t).
  rewrite Ht.
  rewrite (max_comm u t).
  rewrite <- max_assoc.
  rewrite Hu.
  reflexivity.
Qed.

(* [imax] establishes well-behavedness. That is, [imax t] is always
   well-behaved, even if [t] is not. *)

Lemma wb_imax : ∀ t, wb (imax t).
Proof.
  unfold wb. intros. eapply imax_idem.
Qed.

(* -------------------------------------------------------------------- *)

(* More technical results. *)

Lemma le_cocone_refl_imax : ∀ k f,  f k ≤ imax (lim f).
Proof.
  intros. transitivity (lim f).
  { eauto using le_cocone_refl. }
  { eapply self_le_imax. }
Qed.

Lemma le_limiting_imax :
  ∀ f u,
  wb u →
  (∀ k, f k ≤ u) →
  imax (lim f) ≤ u.
Proof.
  intros.
  rewrite <- (imax_le_self u) by assumption.
  eapply imax_mono.
  eauto with le.
Qed.

Lemma max_is_lub :
  ∀ t u v,
  wb v →
  t ≤ v →
  u ≤ v →
  max t u ≤ v.
Proof.
  intros t u v Hv Htv Huv.
  rewrite Htv, Huv.
  apply Hv.
Qed.

Local Instance Proper_equiv_equiv_imax : Proper (equiv ==> equiv) imax.
Proof.
  unfold Proper, respectful.
  split; eauto 6 using imax_mono, equiv_implies_le, equiv_symmetric.
Qed.

Lemma max_imax_left :
  ∀ t u,
  wb u →
  max (imax t) u ≡ imax (max t u).
Proof.
  split.
  + apply max_is_lub.
    { apply wb_imax. }
    { apply imax_mono. apply max_le_left. }
    { rewrite <- self_le_imax. apply max_le_right. }
  + unfold imax at 1. eapply le_limiting.
    induction k; simpl nmax.
    { eapply max_mono_left. eapply self_le_imax. }
    { rewrite IHk; clear IHk.
      rewrite (self_le_imax t) at 2.
      fold (wb (max (imax t) u)).
      eauto using wb_max, wb_imax. }
Qed.

Lemma max_imax_lim_u :
  ∀ f u,
  wb u →
  max (imax (lim f)) u ≡ imax (lim (fun k => max (f k) u)).
Proof.
  intros f u Hu. rewrite <- max_lim_u. eauto using max_imax_left.
Qed.

Lemma under_lim_imax : ∀ t g,  (∀ k, t ≤ g k)  →  t ≤ imax (lim g).
Proof.
  intros. rewrite (under_lim t g) by assumption. apply self_le_imax.
Qed.
